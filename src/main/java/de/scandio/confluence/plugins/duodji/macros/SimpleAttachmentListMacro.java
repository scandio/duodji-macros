package de.scandio.confluence.plugins.duodji.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.ui.AttachmentUiSupport;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

public class SimpleAttachmentListMacro implements Macro {
	
	private static final int MAX_DEFAULT = 10;
	
	private UserAccessor userAccessor;
	private WebResourceUrlProvider webResourceUrlProvider;

	@Override
	public String execute(Map<String, String> parameters, String body,
			ConversionContext context) throws MacroExecutionException {
		
		Page page = (Page) context.getEntity();
		AttachmentUiSupport attachmentUiSupport = new AttachmentUiSupport(webResourceUrlProvider);
		
		int max = MAX_DEFAULT;
		if (StringUtils.isNumeric(parameters.get("max"))) {
			max = Integer.parseInt(parameters.get("max"));
		}
		
		List<Attachment> attachments = getAttachmentList(page, max);

		Map<String, Object> velocityContext = MacroUtils.defaultVelocityContext();
		velocityContext.put("page", page);
		velocityContext.put("attachments", attachments);
		velocityContext.put("attachmentUiSupport", attachmentUiSupport);
	
		return VelocityUtils.getRenderedTemplate("duodji/templates/simpleattachmentlist.vm", velocityContext);
	}
	
	private List<Attachment> getAttachmentList(Page page, int max) {
		List<Attachment> allAttachments = page.getLatestVersionsOfAttachments();
		int safeMax = allAttachments.size() < max ? allAttachments.size() : max;
		return allAttachments.subList(0, safeMax);
	}

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

	public UserAccessor getUserAccessor() {
		return userAccessor;
	}

	public void setUserAccessor(UserAccessor userAccessor) {
		this.userAccessor = userAccessor;
	}


	public WebResourceUrlProvider getWebResourceUrlProvider() {
		return webResourceUrlProvider;
	}

	public void setWebResourceUrlProvider(
			WebResourceUrlProvider webResourceUrlProvider) {
		this.webResourceUrlProvider = webResourceUrlProvider;
	}
	
	


}
