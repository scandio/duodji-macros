package de.scandio.confluence.plugins.duodji.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import jodd.lagarto.dom.Element;
import jodd.lagarto.dom.Text;
import jodd.lagarto.dom.jerry.Jerry;

import java.util.Map;

/**
 * Renders the profile macro with an additional title.
 *
 * @author Georg Schmidl <georg.schmidl@scandio.de>
 */
public class TitledProfileMacro implements Macro {

    protected XhtmlContent xhtmlContent;

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException {
        if (parameters.get("username") == null) {
            throw new MacroExecutionException("Username is required");
        }

        try {
            String userProfileHtml = xhtmlContent.convertStorageToView("<ac:macro ac:name=\"profile\"><ac:parameter ac:name=\"user\">" + parameters.get("username") + "</ac:parameter></ac:macro>", conversionContext);

            if (parameters.get("title") != null) {
                Jerry doc = Jerry.jerry(userProfileHtml);
                String userLink = doc.$("h4").get(0).getInnerHtml();

                Element userDiv = new Element("div");
                userDiv.appendChild(new Text(userLink));

                doc.$("h4").get(0).detachFromParent();

                doc.$(".values").get(0).insertChild(userDiv, 0);

                Element h4 = new Element("h4");
                h4.appendChild(new Text(parameters.get("title")));

                doc.$(".values").get(0).insertChild(h4, 0);
                userProfileHtml = doc.html();
            }

            return userProfileHtml;

        } catch (Exception e) {
            throw new MacroExecutionException("Unable to render profile macro");
        }
    }

    public void setXhtmlContent(XhtmlContent xhtmlContent) {
        this.xhtmlContent = xhtmlContent;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
