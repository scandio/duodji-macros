package de.scandio.confluence.plugins.duodji.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.*;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * Renders a simple space list that can be filtered by space labels.
 *
 * @author Georg Schmidl <georg.schmidl@scandio.de>
 */
public class SimpleSpaceListMacro implements Macro {

    protected LabelManager labelManager;
    protected SpaceManager spaceManager;
    protected PermissionManager permissionManager;
    protected SpaceLogoManager spaceLogoManager;

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        User user = AuthenticatedUserThreadLocal.getUser();
        List<Space> spaces = new ArrayList<Space>();

        if (parameters.get("label") != null && parameters.get("labels") == null) {
            parameters.put("labels", parameters.get("label"));
        }

        if (parameters.get("labels") != null) {

            String[] labelNames = StringUtils.split(parameters.get("labels"), ",");
            List<String> whiteLabels = new ArrayList<String>();
            List<String> blackLabels = new ArrayList<String>();

            for (String labelName: labelNames) {
                if (labelName.startsWith("-")) {
                    labelName = labelName.substring(1, labelName.length());
                    blackLabels.add(labelName);
                } else {
                    whiteLabels.add(labelName);
                }
            }

            if (whiteLabels.isEmpty()) {
                spaces.addAll(spaceManager.getAllSpaces(SpacesQuery.newQuery().withSpaceType(SpaceType.GLOBAL).forUser(user).build()));
            } else {
                for(String labelName: whiteLabels) {
                    Label label = labelManager.getLabel(labelName, Namespace.TEAM);
                    if (label != null) {
                        for (Space space: labelManager.getSpacesWithLabel(label)) {
                            if (permissionManager.hasPermission(user, Permission.VIEW, space)) {
                                spaces.add(space);
                            }
                        }
                    }
                }
            }

            if (!blackLabels.isEmpty()) {
                for(String labelName: blackLabels) {
                    Label label = labelManager.getLabel(labelName, Namespace.TEAM);
                    if (label != null) {
                        for (Space space: labelManager.getSpacesWithLabel(label)) {
                            if (permissionManager.hasPermission(user, Permission.VIEW, space)) {
                                spaces.remove(space);
                            }
                        }
                    }
                }
            }

        } else {
            spaces = spaceManager.getAllSpaces(SpacesQuery.newQuery().withSpaceType(SpaceType.GLOBAL).forUser(user).build());
        }

        Collections.sort(spaces, new Comparator<Space>() {
            @Override
            public int compare(Space space, Space otherSpace) {
                return space.getName().compareTo(otherSpace.getName());
            }
        });
        
        String template = "duodji/templates/simplespacelist.vm";
        if (context.getPropertyAsString("output-device-type") != null &&
                context.getPropertyAsString("output-device-type").equals("mobile")) {
        	template = "duodji/templates/simplespacelist-mobile.vm";
        }

        Map<String, Object> velocityContext = MacroUtils.defaultVelocityContext();
        velocityContext.put("spaceLogoManager", spaceLogoManager);
        velocityContext.put("spaces", spaces);
        return VelocityUtils.getRenderedTemplate(template, velocityContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    public void setLabelManager(LabelManager labelManager) {
        this.labelManager = labelManager;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public void setPermissionManager(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    public void setSpaceLogoManager(SpaceLogoManager spaceLogoManager) {
        this.spaceLogoManager = spaceLogoManager;
    }
}
