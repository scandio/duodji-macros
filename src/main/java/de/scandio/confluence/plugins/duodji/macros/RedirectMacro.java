package de.scandio.confluence.plugins.duodji.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.opensymphony.webwork.ServletActionContext;

import java.util.Map;

/**
 * Redirects the page to the given location. The redirection can be prohibited
 * by adding "noredirect" as GET parameter to the request.
 *
 * @author Georg Schmidl <georg.schmidl@scandio.de>
 */
public class RedirectMacro implements Macro {
    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        boolean noredirect = ServletActionContext.getRequest() != null
                && ServletActionContext.getRequest().getParameter("noredirect") != null;
        boolean preview = "preview".equals(context.getPageContext().getOutputType());

        if (noredirect || preview) {
            return "";
        }

        Map<String, Object> velocityContext = MacroUtils.defaultVelocityContext();
        velocityContext.put("location", parameters.get("location"));
        return VelocityUtils.getRenderedTemplate("duodji/templates/redirect.vm", velocityContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
