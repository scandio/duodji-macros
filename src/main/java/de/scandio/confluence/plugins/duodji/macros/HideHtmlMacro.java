package de.scandio.confluence.plugins.duodji.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import java.util.Map;

/**
 * Hides html tags for a given selector.
 *
 * @author Georg Schmidl <georg.schmidl@scandio.de>
 */
public class HideHtmlMacro implements Macro {
    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        Map<String, Object> velocityContext = MacroUtils.defaultVelocityContext();
        velocityContext.put("selector", parameters.get("selector"));
        return VelocityUtils.getRenderedTemplate("duodji/templates/hidehtml.vm", velocityContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}
