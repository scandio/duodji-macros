package de.scandio.confluence.plugins.duodji.macros;


import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.webresource.WebResourceManager;

import java.util.Map;

/**
 * Hides the children at the bottom of the page.
 *
 * @author Georg Schmidl <georg.schmidl@scandio.de>
 */
public class HideCommentsMacro implements Macro {

    protected WebResourceManager webResourceManager;

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        webResourceManager.requireResourcesForContext("duodji.macros.hidecomments");
        return "";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    public void setWebResourceManager(WebResourceManager webResourceManager) {
        this.webResourceManager = webResourceManager;
    }
}
