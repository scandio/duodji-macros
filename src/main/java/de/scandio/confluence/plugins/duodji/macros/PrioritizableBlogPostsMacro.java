package de.scandio.confluence.plugins.duodji.macros;

import com.atlassian.confluence.macro.ContentFilteringMacro;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.query.BooleanQueryFactory;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.searchfilter.SpacePermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.CreatedSort;
import com.atlassian.confluence.themes.GlobalHelper;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.WikiStyleRenderer;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Prioritizable BlogPosts Macro
 *
 * @author Georg Schmidl <georg.schmidl@scandio.de>
 */
public class PrioritizableBlogPostsMacro extends ContentFilteringMacro {

    private WikiStyleRenderer wikiStyleRenderer;

    @Override
    public boolean hasBody() {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.ALL;
    }

    @Override
    protected String execute(MacroExecutionContext ctx) throws MacroException {
        mergeAlias(ctx, "labels", "label");
        defaultValue(ctx, "labels", "important");

        mergeAlias(ctx, "spaces", "space");
        defaultValue(ctx, "spaces", "@all");

        defaultValue(ctx, "content", "excerpts");
        defaultValue(ctx, "max", "100");

        Map<String, Object> context = MacroUtils.defaultVelocityContext();
        context.put("helper", new GlobalHelper());

        if(prioritizedBlogPostsExist(ctx)) {
            context.put("prioritizedBlogPostsExist", true);
            context.put("negateLabels", negateLabelString(ctx.getParams().get("labels")));
        }
        context.put("labels", ctx.getParams().get("labels"));
        context.put("spaces", ctx.getParams().get("spaces"));
        context.put("content", ctx.getParams().get("content"));
        context.put("max", ctx.getParams().get("max"));

        return VelocityUtils.getRenderedTemplate("duodji/templates/prioritizableblogposts.vm", context);
    }


    private static void mergeAlias(MacroExecutionContext ctx, String param, String alias) {
        if (ctx.getParams().get(alias) != null) {
            ctx.getParams().put(param, ctx.getParams().get(alias));
        }
    }

    private static void defaultValue(MacroExecutionContext ctx, String param, String defaultValue) {
        if (ctx.getParams().get(param) == null) {
            ctx.getParams().put(param, defaultValue);
        }
    }


    protected boolean prioritizedBlogPostsExist(MacroExecutionContext ctx){
        try {

            BooleanQueryFactory query = new BooleanQueryFactory();
            query.addMust(new ContentTypeQuery(ContentTypeEnum.BLOG));

            BooleanQueryFactory spaceKeyQuery = null;
            spaceKeyQuery = spaceKeyParam.findValue(ctx);
            if (spaceKeyQuery != null) {
                query.addMust(spaceKeyQuery.toBooleanQuery());
            }

            BooleanQueryFactory labelQuery = labelParam.findValue(ctx);
            if (labelQuery != null) {
                query.addMust(labelQuery.toBooleanQuery());
            }

            SearchSort searchSort = new CreatedSort(SearchSort.Order.DESCENDING);
            ISearch search = new ContentSearch(query.toBooleanQuery(),
                    searchSort,
                    SpacePermissionsSearchFilter.getInstance(),
                    new SubsetResultFilter(1));

            SearchResults searchResults = searchManager.search(search);
            return searchResults.size() > 0;

        } catch (Exception e) {
            return false;
        }
    }

    protected String negateLabelString(String labels) {
        labels = labels.replace(",", " ");

        List<String> negatedLabels = new ArrayList<String>();
        for (String label: StringUtils.split(labels, null)) {
            negatedLabels.add("-" + label.trim());
        }

        return StringUtils.join(negatedLabels, ",");
    }

    public void setWikiStyleRenderer(WikiStyleRenderer wikiStyleRenderer) {
        this.wikiStyleRenderer = wikiStyleRenderer;
    }
}
